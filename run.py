from app import app


def create_app(config):
    app.config.from_object(config)
    return app



if __name__ == '__main__':
    app = create_app('config')
    app.run(debug=True)
    # print(app.url_map)