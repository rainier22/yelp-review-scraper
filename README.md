# Yelp Review Scraper API

```bash
.
├── README.md                 <-- Instructions file
├── app                 
│   ├── api
|   |   └── __init__.py
|   |   └── models.py         <-- Source code of the Review and Reviewer object
|   |   └── views.py          <-- Source code for the routes
│   ├── integrations
|   |   ├── google
|   |   |   └── vision.py     <-- Google Vision API integration
|   |   └── website.py        <-- Implemantation for scraping the website
│   └── utils
|       └── debugger.py       <-- Debugging tool for the I/Os of the functions
├── flask-env                 <-- Virtual environment of the app
├── config.py                 <-- Source code for environment variables
├── requirements.txt          <-- List of dependencies
└── run.py                    <-- Source code for starting the service
```


## Setup process

**Start the virtual env**

```bash
source flask-env/bin/activate
```

**Install the dependencies**

```bash
pip3 install -r requirements.txt
```

**Set the Google API Key**

```bash
export GOOGLE_API_KEY=<API KEY>
```

**Start Service**

```bash
python3 -m run
```


## Methods

**GET | /api/search**
| Parameters    | Usage                                                      |
| ------------- |:--------------------------------------------------------:  |
| biz-id        | Business ID of the restaurant in Yelp *(Required)*         |
| page          | Page Number where the reviews will be fetched *(Optional)* |
