import re
import os

from ..integrations.website import Yelp
from ..integrations.google.vision import GoogleVisionAPI



class Review(object):
    def __init__(self, reviewer, details):
        self.reviewer = self.__obj_to_dict(object=reviewer)
        self.details = details


    def __obj_to_dict(self, object):
        return object.__dict__


    @classmethod
    def parse_raw_review(cls, raw_review):
        reviewer = Reviewer.parse_element_to_obj(element=raw_review.div.div.div.div.div)
        details = cls.get_review_details_from_element(element=raw_review.div.div.next_sibling)

        return Review(reviewer=reviewer, details=details)


    @classmethod
    def get_review_details_from_element(cls, element):
        return  {
            'rating': Yelp.get_star_rating(element),
            'date': re.findall(r'^(\d*\/\d*\/\d{4}).*', element.text)[0],
            'photos': [],
            'check-in': Yelp.get_number_of_check_in(element),
            'comment': Yelp.get_comment(element),
            'feedback': Yelp.get_feedbacks(element)
        }


class Reviewer(object):
    def __init__(self, name, avatar, location, friends, reviews, photos):
        self.name = name
        self.avatar = avatar
        self.location = location
        self.friends = friends
        self.reviews = reviews
        self.photos = photos
        self.face_annotation = self.get_face_annotation(image=avatar)


    def get_face_annotation(self, image):
        api_key = os.environ.get('GOOGLE_API_KEY')
        vision_api = GoogleVisionAPI(api_key=api_key)
        result = vision_api.annotate_image(image_src=image)
        face_annotation = result[0]['faceAnnotations'][0] if 'faceAnnotations' in result[0] else {}
        keys_to_display = list(filter(lambda x: 'Likelihood' in x, face_annotation.keys()))

        return {k: face_annotation[k] for k in keys_to_display}


    @staticmethod
    def parse_element_to_obj(element):
        raw_text = element.text
        parsed_text = re.findall(r'^((?:\w\s?)+\.)(\D+)', raw_text)[0]

        user_name = parsed_text[0]
        avatar_src = Yelp.find_user_avatar(user_name=user_name, element=element)
        friends = Yelp.find_number_of_attribute(element=element, attribute='friend', name='friend')
        reviews = Yelp.find_number_of_attribute(element=element, attribute='review', name='review')
        photos = Yelp.find_number_of_attribute(element=element, attribute='camera', name='photo')

        return Reviewer(name=user_name,
                        avatar=avatar_src,
                        location=parsed_text[1],
                        friends=friends,
                        reviews=reviews,
                        photos=photos)



if __name__ == '__main__':
    from ..integrations.website import Yelp
    yelp = Yelp()
    business_id = 'spiral-pasay-2'
    page = 1
    reviews = yelp.get_reviews_on_page(business_id=business_id, page=page)

    review = Review.parse_raw_review(raw_review=reviews[1])