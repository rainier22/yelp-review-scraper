from flask import jsonify, request

from . import api
from .models import Review
from ..integrations.website import Yelp


@api.route('/search')
def search():
    yelp = Yelp()
    reviews = []
    data = {'reviews': reviews, 'total': 0}
    business_id = request.args.get('biz-id')
    page_number = int(request.args.get('page', 0))

    if business_id:
        if page_number > 0:
            reviews = yelp.get_reviews_on_page(business_id=business_id, page=page_number)
        else:
            reviews = yelp.get_all_reviews(business_id=business_id)

        data['reviews'] = list(map(lambda x: Review.parse_raw_review(raw_review=x).__dict__, reviews))
        data['total'] = len(reviews)

    return jsonify(data)