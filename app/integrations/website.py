import re
import requests

from urllib.parse import urlencode
from bs4 import BeautifulSoup


class Website(object):
    def __init__(self, url):
        self.url = url


    def get(self, url=None, params={}):
        url = self.url if not url else url
        response = requests.get(url=url, params=params)

        if response.status_code != 200:
            raise Exception('Unable to fetch data from the url')

        return response.content


class Yelp(Website):
    def __init__(self):
        super().__init__('https://yelp.com')


    def get_business_page(self, _id, params={}):
        business_url = f'{self.url}/biz/{_id}'
        content = self.get(url=business_url, params=params)
        return content.decode('UTF-8')


    def _get_number_of_review_pages(self, page_parser):
        result = page_parser.find_all('div', {'aria-label': 'Pagination navigation', 'role': 'navigation'})
        pages = 1

        if result:
            navigation_text = result[0].div.span.text
            if(navigation_text):
                pages = re.findall(r'^Page\s\d*\sof\s(\d*)$', navigation_text)[0]

        return int(pages)


    def get_all_reviews(self, business_id):
        reviews = None
        business = self.get_business_page(_id=business_id)
        page_parser = BeautifulSoup(business, features='html.parser')
        num_of_pages = self._get_number_of_review_pages(page_parser=page_parser)

        for idx in range(num_of_pages):
            reviews_on_page = self.get_reviews_on_page(business_id=business_id, page=idx + 1)
            if reviews:
                reviews.extend(reviews_on_page)
            else:
                reviews = reviews_on_page
        return reviews


    def get_reviews_on_page(self, business_id, page=1):
        print(f'Getting Reviews for business: {business_id} on page {page}')
        business = self.get_business_page(_id=business_id, params={'start': (page-1)*20})
        page_parser = BeautifulSoup(business, features='html.parser')

        def find_review(page_parser):
            return page_parser.body.find_all('li', class_='lemon--li__373c0__1r9wz u-space-b3 u-padding-b3 border--bottom__373c0__uPbXS border-color--default__373c0__2oFDT')

        return find_review(page_parser=page_parser)

    
    @staticmethod
    def find_user_avatar(user_name, element):
        img_element = element.find('img', {'alt': f'Photo of {user_name}'})
        return img_element.attrs['src']

    
    @staticmethod
    def find_number_of_attribute(element, attribute, name):
        value = 0
        attrib_element = element.find('span', {'class': re.compile(f'.*{attribute}.*')})

        if attrib_element:
            text = attrib_element.parent.text
            value = re.findall(r'(\d+) {}s?'.format(name), text)[0]

        return int(value)


    @staticmethod
    def get_star_rating(element):
        rating_element = element.find('div', {'aria-label': re.compile('\d* star rating')})
        return rating_element.attrs['aria-label']


    @staticmethod
    def get_number_of_check_in(element):
        check_in = 0
        check_in_element = element.find_all('span', {'class': re.compile('.*check-in$')})
        
        if check_in_element:
            raw_text = check_in_element[0].parent.text
            check_in_text = re.findall(r'(\d+) check-in', raw_text.strip())
            check_in = check_in_text[0] if check_in_text else check_in

        return int(check_in)


    @staticmethod
    def get_feedbacks(element):
        feedbacks = {}
        buttons = element.find_all('button')
        button_texts = list(map(lambda x: x.text, buttons))

        for text in button_texts:
            splitted_text = text.split(' ')
            feedbacks[splitted_text[0].lower()] = int(splitted_text[1]) if len(splitted_text) > 1 else 0

        return feedbacks

    
    @staticmethod
    def get_comment(element):
        comment_element =  element.find_all('p', {'class': re.compile('.*comment.*')})[0]
        return comment_element.text



if __name__ == '__main__':
    business_id = 'spiral-pasay-2'
    params = {'start': 40}

    yelp = Yelp()

    # yelp.get_business_page(_id=business_id, params=params)
    # reviews = yelp.get_reviews_on_page(business_id=business_id, page=2)
    all_reviews = yelp.get_all_reviews(business_id=business_id)
    print(len(all_reviews))
