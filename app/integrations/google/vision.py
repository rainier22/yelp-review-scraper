import requests
from urllib.parse import urlencode


class GoogleVisionAPI(object):
    def __init__(self, api_key):
        self.api_key = api_key

    @property
    def base_url(self):
        return 'https://vision.googleapis.com/v1/'

    
    def _post(self, url, data):
        result = None

        try:
            response = requests.post(url, json=data)

            if response.status_code != 200:
                raise Exception('Unable to process the request for Google Vision')

            result = response.json()

        except Exception as exception:
            raise exception

        return result


    def annotate_image(self, image_src, features=['FACE_DETECTION']):
        url_path = 'images:annotate'
        payload = {
            "requests": [
                {
                    "image": {
                        "source": {
                            "imageUri": image_src
                        }
                    },
                    "features": list(map(lambda x: 
                        {
                            "type": x,
                            "model": "builtin/stable"
                        }, features))
                }]
        }

        url = self.base_url + url_path + '?' + urlencode({'key': self.api_key})
        result = self._post(url=url, data=payload)

        return result['responses']





if __name__ == '__main__':
    api_key = 'AIzaSyB1VnQeU4KUYQZk01I_BGryKoOnQLKq5Bo'
    image_src = 'https://s3-media0.fl.yelpcdn.com/photo/avg-XIgA056rGHyx45HFYA/60s.jpg'
    vision_api = GoogleVisionAPI(api_key=api_key)
    result = vision_api.annotate_image(image_src=image_src)



