import functools



class Debugger(object):
    @classmethod
    def debug_fxn(cls):
        def decorator_debug_fxn(func):
            @functools.wraps(func)
            def wrapper_debug_fxn(*args, **kwargs):
                print(f"[ { func.__name__.upper() } ] START")
                print(f"[ { func.__name__.upper() } ] ARGS = [ { str(args) } ]")
                print(f"[ { func.__name__.upper() } ] KWARGS = [ { str(kwargs) } ]")

                return_value = func(*args, **kwargs)

                print(f"[ { func.__name__.upper() } ] END")

                return return_value

            return wrapper_debug_fxn
        return decorator_debug_fxn