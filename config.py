import os

basedir = os.path.abspath(os.path.dirname(__file__))

SQLALCHEMY_ECHO = False
SQLALCHEMY_TRACK_MODIFICATIONS = True

GOOGLE_API_KEY = os.environ.get('GOOGLE_API_KEY')